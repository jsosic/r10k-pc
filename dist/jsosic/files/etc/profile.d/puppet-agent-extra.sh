# Add /opt/puppetlabs/puppet/bin to the path for sh compatible users

if [ -z ${PATH-} ] ; then
  export PATH=/opt/puppetlabs/puppet/bin
elif ! echo ${PATH} | grep -q /opt/puppetlabs/puppet/bin ; then
  export PATH=${PATH}:/opt/puppetlabs/puppet/bin
fi
