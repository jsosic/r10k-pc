#
# = Class: jsosic::helpers::docker
#
class jsosic::helpers::docker {

  File {
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }

  package { 'docker-ce': }

  file { '/etc/docker':
    ensure => directory,
  }

  file { '/etc/docker/daemon.json':
    source  => 'puppet:///modules/jsosic/etc/docker/daemon.json',
    require => Package['docker-ce'],
  }

  systemd::dropin_file { 'delete-bridge.conf':
    unit   => 'docker.service',
    source => "puppet:///modules/jsosic/etc/systemd/system/docker.service.d/delete-bridge.conf",
  } ~> service { 'docker':
    ensure  => running,
    enable  => true,
    require => File['/etc/docker/daemon.json'],
  }

}
