#
# = Class: jsosic::helpers::wheel
#
class jsosic::helpers::wheel {

  group { 'wheel': }
  ::sudoers::allowed_command { 'wheel_all':
    command          => 'ALL',
    host             => 'ALL',
    run_as           => 'root',
    group            => 'wheel',
    require_password => false,
    comment          => 'Allow users from wheel group to sudo su -',
  }

}
