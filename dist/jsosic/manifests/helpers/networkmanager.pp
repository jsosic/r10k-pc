#
# = Class: jsosic::helpers::networkmanager
#
class jsosic::helpers::networkmanager {

  include ::desktop::networkmanager
  include ::desktop::networkmanager::gnome
  include ::desktop::networkmanager::vpnc
  include ::desktop::networkmanager::openconnect
  include ::desktop::networkmanager::openvpn
  include ::desktop::networkmanager::l2tp

  file { '/etc/udev/rules.d/85-nm-unmanaged.rules':
    ensure => 'file',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/jsosic/etc/udev/rules.d/85-nm-unmanaged.rules',
  }

}
