#
# = Class: jsosic::helpers::googlechrome
#
class jsosic::helpers::googlechrome {

  include ::desktop::google::chrome
  file { '/usr/share/applications/google-chrome.desktop':
    ensure => 'file',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/jsosic/google-chrome.desktop',
  }

}
