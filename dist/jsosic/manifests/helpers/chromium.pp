#
# = Class: jsosic::helpers::chromium
#
class jsosic::helpers::chromium {

  include ::desktop::chromium
  file { '/usr/share/applications/chromium-browser.desktop':
    ensure => 'file',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/jsosic/chromium-browser.desktop',
  }

}
