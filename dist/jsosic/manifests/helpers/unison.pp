#
# = Class: jsosic::helpers::unison
#
class jsosic::helpers::unison {

  include ::unison

  if $facts['networking']['hostname'] != 'workstation' {
    file { '/home/jsosic/.unison/jsosic.hopto.org.prf':
      mode    => '0644',
      content => template('jsosic/home/jsosic/unison/jsosic.hopto.org.prf.erb'),
    }
  }

}
