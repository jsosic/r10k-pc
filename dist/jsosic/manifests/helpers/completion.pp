#
# = Class: jsosic::helpers::completion
#
class jsosic::helpers::completion {

  File {
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => Package['zsh'],
  }

  # sync completions (avoid changing file modes)
  file { '/usr/share/zsh/site-functions':
    ensure  => directory,
    recurse => 'true',
    mode    => undef,
    source  => 'puppet:///modules/jsosic/zsh/site-functions',
  }

  # gcloud links
  file { '/usr/share/bash-completion/completions/gcloud':
    ensure => 'link',
    target => '/usr/share/google-cloud-sdk/completion.bash.inc'
  }
  file { '/usr/share/zsh/site-functions/_gcloud':
    ensure => 'link',
    target => '/usr/share/google-cloud-sdk/completion.zsh.inc'
  }

}
