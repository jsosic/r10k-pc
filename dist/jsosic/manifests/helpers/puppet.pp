#
# = Class: jsosic::helpers::puppet
#
class jsosic::helpers::puppet {

  class { 'puppet':
    service_ensure => 'stopped',
    service_enable => false,
  }
  include ::puppet::pdk

  File {
    owner => 'root',
    group => 'root',
    mode  => '0644',
  }

  file { '/etc/puppetlabs/puppet/hiera.yaml':
    source => 'puppet:///modules/jsosic/etc/puppetlabs/puppet/hiera.yaml',
  }

  file { '/etc/profile.d/puppet-agent-extra.sh':
    source => 'puppet:///modules/jsosic/etc/profile.d/puppet-agent-extra.sh',
  }

  file { '/usr/local/bin/puppet-apply':
    mode    => '0755',
    content => template('jsosic/puppet-apply.erb'),
  }

}
