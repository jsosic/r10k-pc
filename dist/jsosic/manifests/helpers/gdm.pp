#
# = Class: jsosic::helpers::gdm
#
class jsosic::helpers::gdm {

  File {
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }

  file { '/var/lib/AccountsService/users/gdm':
    content => "[User]\nLanguage=\nXSession=\nSystemAccount=true\n",
  }

  file { '/var/lib/AccountsService/users/jsosic':
    content => "[User]\nLanguage=\nXSession=\nSystemAccount=true\n",
  }

  package { [ 'gnome-session-xsession', 'gnome-classic-session' ]:
    ensure => absent
  }

  file { '/etc/profile.d/libreoffice.sh':
    content => "# CentOS 7 GTK3 library is not compatible to LibreOffice\nexport SAL_USE_VCLPLUGIN=gtk\n",
  }

}
