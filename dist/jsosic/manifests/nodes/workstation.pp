#
# = Class: jsosic::nodes::workstation
#
class jsosic::nodes::workstation {

  include ::jsosic
  include ::tools::zfs

  include ::desktop::kmod::nvidia

  # iptables
  class { '::tools::iptables':
    ipv4_source => 'puppet:///modules/jsosic/iptables',
  }

  include ::nginx
  file { '/var/www': ensure => directory }
  ::nginx::vhost { 'default' :
    listen     => '80',
    docroot    => '/var/www/html',
  }
  # system_u:object_r:httpd_sys_content_t:s0
  file { '/etc/nginx/conf.d/proxy.conf':
    source  => 'puppet:///modules/jsosic/proxy.conf',
    require => Package['nginx'],
  }

}
