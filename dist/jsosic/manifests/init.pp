#
# = Class: jsosic
#
class jsosic (
  $mitogen_version = '0.2.9',
  $shell_aliases   = [],
){

  Package { allow_virtual => true }
  stage {'yumsetup': before => Stage['main'] }

  class { '::yum':                         stage => 'yumsetup', }
  class { '::yum::repo::base':             stage => 'yumsetup', }
  class { '::yum::repo::base::extras':     stage => 'yumsetup', }
  class { '::yum::repo::base::sclorh':     stage => 'yumsetup', }
  class { '::yum::repo::epel':             stage => 'yumsetup', }
  class { '::yum::repo::ius':              stage => 'yumsetup', }
  class { '::yum::repo::puppet':           stage => 'yumsetup', }
  class { '::yum::repo::nginx':            stage => 'yumsetup', }
  class { '::yum::repo::srce':             stage => 'yumsetup', }
  class { '::yum::repo::virtualbox':       stage => 'yumsetup', }
  class { '::yum::repo::nux::dextop':      stage => 'yumsetup', }
  class { '::yum::repo::slack':            stage => 'yumsetup', }
  class { '::yum::repo::google::cloudsdk': stage => 'yumsetup', }
  class { '::yum::repo::dockerce':         stage => 'yumsetup', }
  class { '::yum::repo::elrepo::kernel':   stage => 'yumsetup', }
  yum::copr { 'dsommers/openvpn-release': }
  yum::copr { 'dsommers/openvpn3': }

  file { '/root/.bashrc':
    content => template('jsosic/root/bashrc.erb'),
  }

  # ca certificates
  $ca_certs = lookup('jsosic::ca_certs', undef, undef, [])
  $ca_certs.each |$ca_cert| {
    ca::trust::add { $ca_cert['path']:
      content => "${ca_cert['cert']}\n",
    }
  }

  # cli tools
  include ::java::openjdk8
  include ::admintools
  include ::tools::git
  include ::tools::git::lfs
  include ::tools::git::crypt
  include ::tools::tig
  include ::tools::hub
  include ::tools::rpmlint
  include ::tools::dpkg
  include ::tools::mercurial
  include ::tools::qrencode
  include ::tools::imagemagick

  include ::tools::pmount
  include ::tools::mssys
  include ::tools::ntfsprogs
  include ::tools::ntfs3g
  include ::tools::hddtemp
  include ::tools::mlocate
  include ::tools::p7zip
  include ::tools::pbzip2
  include ::tools::unrar
  include ::tools::whois
  include ::tools::pwgen
  include ::tools::pv
  include ::tools::perf
  include ::tools::stress
  include ::tools::ltrace
  include ::tools::ag
  include ::tools::jq
  include ::tools::colordiff
  include ::postgresql::client

  include ::tools::dcfldd
  include ::tools::socat
  include ::tools::powertop
  include ::tools::hdparm
  include ::tools::sdparm
  include ::tools::aspell
  include ::tools::nethogs

  include ::tools::awscli
  include ::tools::gcloudsdk
  include ::tools::rclone

  include ::tools::siege

  include ::tools::groovy

  include ::tools::proxychains

  include ::jsosic::helpers::completion

  # ansible
  include ::tools::ansible
  include ::tools::yamllint
  include ::python::mod::prettytable
  include ::python::mod::netaddr
  include ::python::mod::libcloud
  include ::python::mod::google_api_client
  # daemonizing script
  include ::python::mod::daemon
  # general python
  include ::python::mod::virtualenv

  # desktop stuff
  # boot splash
  include ::plymouth
  # DE
  # include ::desktop
  include ::desktop::de::xfce4
  include ::desktop::pavucontrol
  # libre office
  include ::desktop::libreoffice
  # browser/mua
  include ::desktop::firefox
  include ::desktop::thunderbird
  include ::jsosic::helpers::googlechrome
  include ::jsosic::helpers::chromium
  # unison sync tool with config
  include ::jsosic::helpers::unison

  include ::desktop::freerdp
  include ::desktop::virtviewer
  include ::desktop::gimp
  include ::desktop::okular
  include ::desktop::fbreader
  include ::desktop::k3b
  include ::desktop::keepassx
  include ::desktop::redshift
  include ::desktop::kile
  include ::desktop::pidgin  # !!! TODO !!! => compile pidgin plugins
  # include ::desktop::xbacklight  TODO !!! => compile :(
  include ::desktop::galculator
  # include ::desktop::xbindkeys   TODO !!! => compile :(
  # include ::desktop::grdesktop   TODO !!! => compile :(
  include ::desktop::qiv
  include ::desktop::xclip
  include ::desktop::xlockmore
  include ::desktop::smplayer
  include ::desktop::qbittorrent
  include ::desktop::rtorrent
  include ::desktop::rxvt
  include ::desktop::slack
  include ::desktop::youtubedl

  include ::desktop::wireshark

  include ::virtualbox
  include ::vagrant
  include ::jsosic::helpers::docker

  include ::jsosic::helpers::wheel
  include ::jsosic::helpers::puppet
  include ::jsosic::helpers::gdm
  include ::jsosic::helpers::networkmanager

  package { 'qemu-kvm': }

  archive { "/usr/local/src/mitogen-${mitogen_version}.tar.gz":
    source       => "https://networkgenomics.com/try/mitogen-${mitogen_version}.tar.gz",
    extract      => true,
    extract_path => '/usr/local/src',
    creates      => "/usr/local/src/mitogen-${mitogen_version}",
    cleanup      => false,
  }

  file { '/etc/profile.d/ansible_mitogen.sh':
    mode    => '0644',
    content => template('jsosic/etc/profile.d/ansible_mitogen.sh.erb'),
  }

  file { '/etc/profile.d/freetype27-truetype-fix.sh':
    mode    => '0644',
    content => template('jsosic/etc/profile.d/freetype27-truetype-fix.sh.erb'),
  }

  ::sysctl { 'net.ipv6.conf.default.disable_ipv6':
    value   => '1',
    comment => 'Disable IPv6 by default on all new interfaces.',
  }

  ::sysctl { 'net.ipv6.conf.all.disable_ipv6':
    value   => '1',
    comment => 'Disable IPv6 on all interfaces.',
  }

  # resolvconf
  file { '/etc/hosts.d':
    ensure => directory,
  }
  include resolvconf
  include dnsmasq

}
