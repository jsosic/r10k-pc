# r10k PC

This repository is the [r10k](https://github.com/adrienthebo/r10k) control
repository for the [jsosic's workstation and laptop](https://jsosic.wordpress.com)
infrastructure.

## How to initialize node

Shell script is prepared in this repo for node initialization.

Create a clean minimal desktop vanilla install of CentOS 7 x86_64 and run the script:

```
sh scripts/prepare
```
After script is finished, upload content of `/root/.ssh/id_key.pub` to
[Deploy Keys](https://gitlab.com/jsosic/r10k-pc/-/settings/repository#js-deploy-keys-settings), and
afterwards run:
```
r10k deploy environment master -p -v
```

After code is deployed, puppet manifests can be applied by running

```
puppet apply -tv -e 'include ::jsosic::nodes::x230' --environment master
```

After initial run, there will be a `puppet-apply` helper script installed, so you can use
short version:

```
puppet-apply -e master
```
